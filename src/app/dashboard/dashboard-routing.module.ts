import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './default/default.component';
import { ProfileFormComponent } from './profile-form/profile-form.component';


export const routes: Routes = [
{path : '', component : DefaultComponent },
{path : 'setting', component: ProfileFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
