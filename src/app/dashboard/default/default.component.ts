import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/_shared/services/http.service';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.css']
})
export class DefaultComponent implements OnInit {
  id;
  today: number = Date.now();
  countData: any=[] ;

  constructor(private http: HttpService ) { }

  ngOnInit() {

    this.countData = {
      total: 0,  
      today:0,
      tomorrow: 0,
      monthly: 0
    }


    this.id = localStorage.getItem('token');
    console.log("token is ", this.id);

    this.countOfCase();
  }
  countOfCase() {
    this.http.simpleGet('case/totalcase/' + this.id) //'user/?id='+id
      .subscribe((res: any) => {
        // this.loading=false;
        console.log("Getting court Data", res);
        console.log(this.countData)

        this.countData = {
          total: res.doc.totalCaseCount,  
          today:res.doc.todayCaseCount[0].count,
          tomorrow: res.doc.tommorowCaseCount[0].count,
          monthly: res.doc.monthlyCaseCount[0].count
        }
        console.log(this.countData);

      }, err => {
        console.log(err);
      })
  }

 


}
