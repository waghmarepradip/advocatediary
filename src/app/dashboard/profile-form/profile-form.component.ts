import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from '../../_shared/services/http.service';
import { ToastrService } from 'ngx-toastr';
// import { } from '../../../assets/img/download.png'
@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.css']
})
export class ProfileFormComponent implements OnInit {
  selectedFile: File = null;
  imageUrl = "../../../assets/img/download.png"
  fileToUpload: File = null;
  public loading = false;
  fd = new FormData();
  
  constructor(  private http: HttpService,private toastr: ToastrService) { }

  ngOnInit() {
  }
  onFileSelected(event) {
    console.log(event);
    this.selectedFile = <File>event.target.files[0];
    this.fd.append('image', this.selectedFile);
    console.log(event.target.files);
    console.log(this.selectedFile.name);
  }
  
  setImage(){
console.log("hiiiiiiiiiiiiiiii");
    this.loading = true;
    let id = "5d68b864fa673e1132c990ff";
    // console.log(this.loginForm.value)
    this.http.put('register/'+id, this.fd, ) .subscribe(
      res => {
        console.log("Login Successfull", res)
        this.loading = false;
        this.fd.append('image','');
        this.toastr.success('Update Your Profile');
        // this.fd.append('image',)
       
      }
      , err => {
        console.log(err)
        this.loading = false;
        this.toastr.error('try again');
        // this.route.navigate(['/errorPage'])
      })




  }

  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }


}
