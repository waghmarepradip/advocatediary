import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from 'src/app/_shared/services/http.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  customerForm: FormGroup;
  fileUpload: boolean = false;
  modalRef: BsModalRef;
  file: any;
  fileData: FormData;
  loading=false;
  submitted: boolean = false;
  constructor(private fb: FormBuilder, private http: HttpService,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.customerForm = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      phoneNo: ['',[Validators.required,Validators.maxLength(2)]],
      email: ['', [Validators.required, Validators.email]],
      address: ['', [Validators.required]],
    })


  }
  openModal() {
    if (this.fileUpload)
      this.fileUpload = false;
    else
      this.fileUpload = true
  }

  // openModal(template: TemplateRef<any>) {
  //   console.log("Openning modal")
  //   this.modalRef = this.modalService.show(template);
  // }
  // hideModal() {
  //   this.modalRef.hide()
  // }


  // convenience getter for easy access to form fields
  get validate() { return this.customerForm.controls; }

  submit() {
    this.loading=true;
    this.submitted = true;
    if (this.customerForm.invalid) {
      this.loading=false;
      return;
    }
    this.http.post('client', this.customerForm.value).subscribe(res => {
      console.log("Customer Entry saved Successfully..", res);
      this.loading=false;
    }, err => {
      console.log(err);
      this.loading=false;
    })


    console.log(this.customerForm.value)
  }

  upload(event) {
    this.loading=true;
    console.log(event.target.files[0]);
    this.file = <any>event.target.files[0]
    this.fileData = new FormData()
    this.fileData.append('file', this.file);
    console.log(this.fileData);
    this.http.post('client/bulkdata', this.fileData).subscribe(res => {
      console.log("File Upload", res);
      this.loading=false;
    }, err => {
      console.log(err);
      this.loading=false;
    })

  }


}
