import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpService } from 'src/app/_shared/services/http.service';
import { Params, Route, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  id: any;
  customerForm: FormGroup;
  submitted: boolean = false;
  clientData:any;
  loading=false;
  constructor(private fb: FormBuilder, private http: HttpService,
    private route: ActivatedRoute) { }

  ngOnInit() {


    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];                               // To get Id from active routes
      console.log("Constructor==>", this.id)
    });
    this.getClientData();
    this.customerForm = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      phoneNo: ['', [Validators.required, Validators.pattern('[0-9]+')]],
      email: ['', [Validators.required, Validators.email]],
      address: ['', [Validators.required]],
    })


  }

  getClientData(){
    this.loading=true;
    this.http.simpleGet('client/?id='+this.id)
    .subscribe((res:any)=>{
      console.log("Get Client By id",res);
      this.clientData=res.doc;
      console.log(this,this.clientData);
      this.patchValuToForm() ;
      this.loading=false; 

    },err=>{
      console.log(err);
    })
  }

  patchValuToForm() {
    console.log("patch method")
    this.customerForm.patchValue(this.clientData);                      // Function for patching the value to the forms field

  }

  // convenience getter for easy access to form fields
  get f() { return this.customerForm.controls; }

  submit() {
    this.loading=true;
    this.submitted = true;
    if (this.customerForm.invalid) {
      return;
    }
    this.http.put('client/?id='+this.id, this.customerForm.value).subscribe(res => {
      console.log("Customer Entry saved Successfully..", res);
      this.loading=false;
    }, err => {
      console.log(err)
    })


    console.log(this.customerForm.value)
  }



}
