import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/_shared/services/http.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  searchText;
  clientData: any;
  loading=false;
  constructor(private http: HttpService) { }

  ngOnInit() {
    this.getClientdata()

  }

  getClientdata() {
    this.loading=true
    this.http.simpleGet('client').subscribe(
      (res: any) => {
        console.log("Getting Data", res);
        this.clientData = res.doc;
        this.loading=false;
      }
    ),err=>{
      console.log(err);
      this.loading=false;
    }
  }

  deleteClient(id) {
    this.loading=true;
    console.log(id);
    this.http.delete("client/?id="+id).subscribe(
      res => {
        console.log("Client Deleted Successfully", res);
        this.getClientdata();
        this.loading=false;
      }, err => {
        console.log(err);
        this.loading=false;
      }
    )
  }


}
