import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  imageUrl = "../../../assets/lawyer.png";
  sideMenu: any = [];
  // caseMenu: any = [];
  // fields: any = [];
  Case: any =[];
  Court: any =[];
  Client : any =[];
  User: any =[];
  profile:any;
  constructor(private route: Router, ) { }

  ngOnInit() {
    this.profile=JSON.parse(localStorage.getItem('profile'));

this.Case.push({class:'fas fa-briefcase',menu : 'Add',rougth : 'case/add'});
this.Case.push({class:'fas fa-address-card',menu : 'list',rougth : 'case/list'});

this.Court.push({class:'fas fa-briefcase',menu : 'Add',rougth : 'court/add'});
this.Court.push({class:'fas fa-address-card',menu : 'list',rougth : 'court/list'});

this.Client.push({class:'fas fa-briefcase',menu : 'Add',rougth : 'client/add'});
this.Client.push({class:'fas fa-address-card',menu : 'list',rougth : 'client/list'});

this.User.push({class:'fas fa-briefcase',menu : 'Add',rougth : 'user/add'});
this.User.push({class:'fas fa-address-card',menu : 'list',rougth : 'user/list'});
// this.Case.push({class:'fas fa-briefcase',menu : 'Add',rougth : 'case/add'});






    //  this.caseMenu.push({class:'fas fa-file-alt',menu : ' All Cases ' , rougth : 'case/list'});
    //  this.caseMenu.push({class:'fas fa-file-alt',menu : 'Todays Cases' , rougth : ''})
    //  this.caseMenu.push({class:'fas fa-file-alt',menu : 'Tomorrows Cases' , rougth : ''})
    //  this.caseMenu.push({class:'fas fa-file-alt',menu : 'Date Awaited Cases',rougth : ''})

    //  this.fields.push({class:'fas fa-briefcase',menu : 'Case Type Master'});
    //  this.fields.push({class:'fas fa-clipboard-list',menu : 'P.S/Company Master'})
    //  this.fields.push({class:'fas fa-briefcase',menu : 'Advocate Master' ,rougth: 'advocate/add'});
    //  this.fields.push({class:'fas fa-chess',menu : 'Caurt Master' , rougth : 'court/add'})



    this.sideMenu.push({ class: 'fas fa-book-reader', menu: ' Dashboard', rougth: 'dashboard', })
    // this.sideMenu.push({ class: 'fas fa-briefcase', menu: ' Add Fields', rougth: 'dashboard', subMenu: this.fields, subMenuStatus: true })
    this.sideMenu.push({ class: 'fas fa-briefcase', menu: ' Case', rougth: 'case/add' , subMenu: this.Case, subMenuStatus: true })
    this.sideMenu.push({ class: 'fas fa-chess', menu: ' Court', rougth: 'court/add' , subMenu: this.Court, subMenuStatus: true })
    this.sideMenu.push({ class: 'far fa-file-video', menu: ' Client', rougth: 'client/add' , subMenu: this.Client, subMenuStatus: true })
    this.sideMenu.push({ class: 'fas fa-address-card', menu: ' User', rougth: 'user/add' , subMenu: this.User, subMenuStatus: true })
    // this.sideMenu.push({ class: 'fas fa-file-alt', menu: 'Case ', rougth: 'case/list', subMenu: this.caseMenu, subMenuStatus: true })
    this.sideMenu.push({ class: 'fas fa-cogs', menu: ' Setting ', rougth: 'dashboard/setting', })


    // this.sideMenu.push({class:'fas fa-address-card',menu : 'About' , })
    // this.sideMenu.push({class:'fas fa-phone',menu : 'Contact' ,  })


    console.log(this.sideMenu, "side menu");
  }


}
