import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { AuthlayoutComponent } from '../authlayout/authlayout.component'
import { from } from 'rxjs';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  ngOnInit() {
 
  }
  constructor(private http : HttpService ,
     private alc : AuthlayoutComponent,
     private router:Router){

  }

  today: number = Date.now();

  routing(value){
    console.log(value,"ok");
    
  }
  navBarDisplay(){
  
    this.http.chekLogIn();
    this.alc.display();
    }

    logout()
    {
      localStorage.clear();
      this.router.navigate(['/home']);
    }
}
