import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service';
// import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-authlayout',
  templateUrl: './authlayout.component.html',
  styleUrls: ['./authlayout.component.css'],

})
export class AuthlayoutComponent implements OnInit {
  navBar: boolean;
  status: boolean = false;
  RouterDisplay: any;
  verify: boolean = false;
  name;

  constructor(private http: HttpService) {

  }

  ngOnInit() {
    if (localStorage.getItem('token')) {
      this.status = true;
      console.log(localStorage.getItem('token'))
      let profile: any = JSON.parse(localStorage.getItem('profile'));
      console.log(profile);
      this.name=localStorage.getItem('token') ;

      if (profile.status) {
        console.log("Correct");
        this.verify = true;
      }
    }
    this.display();
    // this.defaultComponentDisplay();
  }
  display() {
    console.log("display", this.navBar)
    this.navBar = this.http.navBar;
    this.RouterDisplay = {
      "navSuccess": this.navBar,
      "navFalse": !this.navBar
    }

  }
 
  // mailVerifyVar;
  // defaultComponentDisplay(){
  //   this.mailVerifyVar = this.http.mailVerifyVar;
  //   console.log("mail" , this.mailVerifyVar);
  // }


}
