import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { TouchSequence } from 'selenium-webdriver';
@Injectable({
  providedIn: 'root'
})
export class HttpService {
  apiUrl: string;
  headers: HttpHeaders;
  constructor(private http: HttpClient) { 
    this.apiUrl = "http://192.168.0.129:4000"
    this.headers = new HttpHeaders();
  }


  post(url: string, data?: any, params?: object) {
    console.log("IN SERVICE==>", data)
    const apiUrl = `${this.apiUrl}/${url}${this.generateQueryString(params)}`;
    console.log(data)
    return this.http.post<any>(apiUrl, data);
  }

  generateQueryString(params?: object): string {
    let queryString = '',
      httpParam = new URLSearchParams();
    Object.keys(params || {}).forEach(key => httpParam.set(key, params[key]));
    queryString = httpParam.toString() ? `?${httpParam.toString()}` : '';
    return queryString;
  }

  simpleGet(url: string) {
    return this.http.get(`${this.apiUrl}/${url}`);
  }

  /**
   * delete method does not have any body part
   * passes object id as parameter
   * also passes token in header part
   * @param url : url where request will be send
   */
  delete(url: string) {
    const apiUrl = `${this.apiUrl}/${url}`;
    return this.http.delete(apiUrl, {
      headers: this.headers
    });
  }

  put(url: string, data?: any, params?: object) {
   
    const apiUrl = `${this.apiUrl}/${url}${this.generateQueryString(params)}`;
    console.log("In service data=",data )
    console.log("In service",apiUrl )
    return this.http.put(apiUrl, data, {
      headers: this.headers
    });
  }


 navBar : boolean = true;
  chekLogIn(){
    this.navBar = !this.navBar;
  }

}






