import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { catchError, filter, take, switchMap } from "rxjs/operators";
import { throwError, Observable, BehaviorSubject, of } from "rxjs";
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(private injector : Injector,   private route: Router) {

   }
   intercept(req, next){
    let token = localStorage.getItem('token');
    console.log("In Interceptor service ${token}",token);
      let tokenizedReq = req.clone({
      setHeaders:{
        Authorization :`Bearer ${token}`
      }
    })
    console.log("Tokenrequiest",tokenizedReq)
    return next.handle(tokenizedReq).pipe(
    
      catchError((error: HttpErrorResponse) => {
        // if (error.status !== 401)
        if (error.status === 401 ) {
          console.log("chu ......................");
          this.route.navigate(['home/login']);
          // 401 handled in auth.interceptor
          // this.toastr.error(error.message);      
        }
        return throwError(error);
      })
    );
  }

 
  

}
