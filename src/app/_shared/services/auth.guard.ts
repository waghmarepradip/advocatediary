import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service'
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  token;
  result: boolean;
  constructor(private http: HttpService,
    private route: Router) { }
  canActivate(callback) {
    let token;
    console.log(localStorage.getItem('token'), "token Send ");
    if(localStorage.getItem('token')){
         return true;
       }else{
        this.route.navigate(['home/login']);
      return false;
    }
    }
}


