import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from 'src/app/_shared/services/http.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  courtForm:FormGroup;
  loading=false;
  submitted=false;
  constructor(private fb:FormBuilder,
              private http:HttpService) { }

  ngOnInit() {
    this.courtForm=this.fb.group({
      courtName:['',Validators.required],
      courtType:['',Validators.required],
      location:['',Validators.required]
    })
  }

   // convenience getter for easy access to form fields
   get validate() { return this.courtForm.controls; }

  submit(){
     this.submitted=true;
    if(this.courtForm.invalid)
    {
      return;
    }
    // this.loading=true;
    console.log(this.courtForm.value)
    // this.http.post('court',this.courtForm.value)
    // .subscribe(res=>{
    //   console.log("Court Entry Added Successfully",res);
    //   this.loading=false;
    // },err=>{
    //   console.log(err);
    //   alert(err.error.text);
    //   this.loading=false;
      
    // })
  }

}
