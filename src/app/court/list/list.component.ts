import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/_shared/services/http.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  searchText;
  court:any;
  private loading=false;
    constructor(private http:HttpService) { }
  
    ngOnInit() {
      this.getCourtData()
    }
  
  getCourtData(){
    this.loading=true;
    this.http.simpleGet('court')
    .subscribe((res:any)=>{
      console.log("Getting Court Data",res)
    this.court=res.doc;
    console.log(this.court);
    this.loading=false;
    },err=>{
      console.log(err);
      this.loading=false;
  
    })
  }
  
  deleteCourtEntry(id){
    this.loading=true;
    console.log(id);
    this.http.delete('court/?id='+id)
    .subscribe(res=>{
      console.log("Record Deleted Successfully..",res);
      this.loading=false;
      this.getCourtData()
  
    },
    err=>{
      console.log(err);
      this.loading=false;
  
    })
  }
  
  }
  