import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpService } from 'src/app/_shared/services/http.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  courtForm:FormGroup;
  id;
  private loading=false;
  courtData:any;
  constructor(private fb:FormBuilder,
              private http:HttpService,
              private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      console.log("Constructor==>", this.id)
    })

    this.getCourtData()
    

    this.courtForm=this.fb.group({
      courtName:[''],
      courtType:[''],
      location:['']
    })
  }


  getCourtData(){
    this.loading=true
    this.http.simpleGet('court/?id='+this.id)
    .subscribe((res:any)=>{
      this.loading=false;
      console.log("Getting court Data",res);
      this.courtData=res.doc;
      console.log(this.courtData);
      this.patchValuToForm();

    },err=>{
      console.log(err);
    })
  }

  patchValuToForm(){
    this.courtForm.patchValue(this.courtData);
  }

  submit(){ 
    this.loading=true
    console.log(this.id)
    console.log(this.courtForm.value);
    this.http.put('court/?id='+this.id,this.courtForm.value)
    .subscribe(res=>{
      console.log("Data Updated",res);
      this.loading=false

    },err=>{
      console.log(err); 
      this.loading=false;

    })
  }

}
  