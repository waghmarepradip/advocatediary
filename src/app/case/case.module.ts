import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CaseRoutingModule } from './case-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { ViewComponent } from './view/view.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import { TagInputModule } from 'ngx-chips'; 
import { NgxLoadingModule } from 'ngx-loading';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { BsDatepickerModule, ModalModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
// import { SweetAlertService } from 'ngx-sweetalert2';


@NgModule({
  declarations: [ListComponent, AddComponent, EditComponent, ViewComponent],
  imports: [
    CommonModule,
    CaseRoutingModule,
    FormsModule,
    TagInputModule,
    NgxLoadingModule.forRoot({}),
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    BsDatepickerModule.forRoot(),
    TypeaheadModule,
    ModalModule,
    ToastrModule.forRoot(),
    // SweetAlertService

    
  
  ]
})
export class CaseModule { }
