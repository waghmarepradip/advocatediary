import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Params, ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/_shared/services/http.service';
import { ToastrService } from 'ngx-toastr'


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  caseForm: FormGroup;                                      //  Case Form variable 
  id;                                                      // varible  to store the param id
  caseData: any;                                          // variable to store the case data
  courtData: any;                                        // variable to store the court data which helps in drop down of court name
  loading: boolean;                                     // variale for loading animation
  oppositePartyNamesArray: any = [];                   // variable to store the names of the opposite party 
  courtId: any;                                       // variable to store the courtId
  token: string;
  courtNameArray = [];                              // variable to store the token
  clientData;
  clientId: any;
  minDate: Date;
  date:Date;

  clientNameAndPhoneNumberArray: any = [];

  constructor(private fb: FormBuilder,
    private http: HttpService,
    private route: ActivatedRoute,
    private toastr: ToastrService) { }

  ngOnInit() {

    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate() + 1);

    if (localStorage.length > 0) {        // To check the local storage is empty or not
      // We have items
      console.log("Data found")

      this.token = localStorage.getItem("token");
      console.log(this.token);
    } else {
      // No items
      console.log("No data")
    }

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];                               // To get Id from active routes
      console.log("Constructor==>", this.id)
    });
    this.getCaseData();                                     // function call for getting the case data
    this.getCourtData()                                     // function call for getting the court data
    this.getClientData()
    this.caseForm = this.fb.group({
      caseId: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      caseName: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      courtId: ['', Validators.required],
      oppositeParty: ['', Validators.required],
      caseType: ['', Validators.required],
      caseStatus: ['', Validators.required],
      clientId: [''],
      nextDate: ['']

    })

  }
  // convenience getter for easy access to form fields
  get validate() { return this.caseForm.controls; }


  selectCourtId(data) {
    if (data.id === "clientId") {
      this.setClientId(data)
    } else {
      this.setCourtId(data);
    }
  }

  setCourtId(id) {
    for (var i = 0; i < this.courtData.length; i++) {
      if (this.courtData[i].courtName === id) {
        console.log(this.courtData[i]._id);
        this.courtId = this.courtData[i]._id;
      }
    }

  }
  setClientId(data) {
    console.log("In Client")
    let id = data.value;
    console.log(id);
    let phoneNo = [] = id.split(' ');
    console.log(phoneNo[phoneNo.length - 1]);

    for (var i = 0; i < this.clientData.length; i++) {
      if (this.clientData[i].phoneNo === parseInt(phoneNo[phoneNo.length - 1])) {
        console.log(this.clientData[i]._id);
        this.clientId = this.clientData[i]._id;
        console.log(this.clientId)
      }
    }

  }
  getClientData() {
    this.loading = true;
    this.http.simpleGet('client').subscribe((res: any) => {
      console.log("getting Client data", res);
      this.clientData = res.doc;
      for (let i = 0; i < this.clientData.length; i++) {
        this.clientNameAndPhoneNumberArray.push(this.clientData[i].name + " " + this.clientData[i].phoneNo)
      }
      console.log(this.clientNameAndPhoneNumberArray);
      this.loading = false;

    }, err => {
      console.log(err);

    })
  }


  getCourtData() {
    this.loading = true;
    this.http.simpleGet('court')                          // Function of getting the court data
      .subscribe((res: any) => {
        console.log("Getting Court Data", res);
        this.courtData = res.doc;
        for (let i = 0; i < this.courtData.length; i++) {
          this.courtNameArray.push(this.courtData[i].courtName);
        }
        console.log(this.courtNameArray);
        this.loading = false;
      }, err => {
        console.log(err);
      })
  }



  addOppositePartyNames(value) {                     // Function for to push the tag-input values into the oppositePartyName array
    this.oppositePartyNamesArray.push(value);
    console.log(value);
    console.log(this.oppositePartyNamesArray);
  }

  removeOppositePartyNames(value) {
    console.log(value);
    for (var i = 0; i < this.oppositePartyNamesArray.length; i++) {  // Function for to splice the tag-input values into the                                                                                oppositePartyName array
      if (this.oppositePartyNamesArray[i] === value) {
        this.oppositePartyNamesArray.splice(i, 1);
        console.log(this.oppositePartyNamesArray);
      }
    }
  }


  getCaseData() {
    this.loading = true
    this.http.simpleGet('case/?id=' + this.id)
      .subscribe((res: any) => {
        this.loading = false;                                     //Function for getting the case data
        console.log("Getting case Data", res);
        this.caseData = res.doc;
        console.log(this.caseData);
        this.patchValueToForm();

      }, err => {
        console.log(err);
      })
  }
  // // Function for patching the value to the forms field

  patchValueToForm() {
    console.log("In patch ")
    this.caseForm.patchValue(this.caseData)
    this.caseForm.patchValue({ courtId: this.caseData.courtId.courtName });
    this.caseForm.patchValue({ clientId: this.caseData.clientId.name })
    this.date=this.caseData.nextDate;
    console.log(this.date)
    this.oppositePartyNamesArray = this.caseForm.value.oppositeParty;

    console.log(this.caseForm.value)
  }

  // Function for submiting the data to the server

  submit() {
    this.loading = true;
    this.caseForm.value.oppositeParty = this.oppositePartyNamesArray;
    this.caseForm.value.courtId = this.courtId;
    this.caseForm.value.clientId = { "name": this.caseData.clientId }
    console.log(this.caseForm.value);
    this.http.put('case/' + this.id, this.caseForm.value)
      .subscribe((res: any) => {
        console.log("Update SuccessFully", res);
        this.toastr.success(res.message)
        this.loading = false;
      },
        err => {
          console.log(err);
          this.loading = false;
          this.toastr.error(err.message)
        })
  }

}
