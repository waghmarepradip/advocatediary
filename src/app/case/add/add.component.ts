import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/_shared/services/http.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr'
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  selected: string;
  public loading = false;
  minDate: Date;
  caseForm: FormGroup;                            //  Case Form variable 
  courtId;
  courtNameArray = []                                       // variable to store the courtId
  oppositePartyNamesArray: any = [];               // variable to store the names of the opposite party 
  courtData: any;
  submitted: boolean = false;                         // variable to store the court data which helps in drop down of court name
  token: any;
  clientData: any;
  clientId: any;
  clientNameAndPhoneNumberArray: any = [];


  constructor(private fb: FormBuilder,
    private http: HttpService,
    private toastr: ToastrService) {

  }

  ngOnInit() {


    if (localStorage.length > 0) {
      // We have items
      console.log("Data found")                      // To check the local storage is empty or not

      this.token = localStorage.getItem("token");
      console.log(this.token);
    } else {
      // No items
      console.log("No data")
    }


    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate() + 1);


    this.getCourtData();
    this.getClientData();
    this.caseForm = this.fb.group({
      caseId: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      caseName: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      courtId: ['', [Validators.required]],
      oppositeParty: ['', Validators.required],
      nextDate: ['', Validators.required],
      caseType: ['', [Validators.required]],
      caseStatus: ['', [Validators.required]],
      advocateRefId: [''],
      clientId: ['',[Validators.required]]

    })

  }

  // convenience getter for easy access to form fields
  get validate() { return this.caseForm.controls; }


  selectCourtId(data) {
  }

  setCourtId(data) {

    console.log(data);
    console.log(this.caseForm.value.courtId);
    console.log("In Court")
    let id = data.value
    for (var i = 0; i < this.courtData.length; i++) {
      if (this.courtData[i].courtId === id) {
        console.log(this.courtData[i]._id);
        this.caseForm.value.courtId = this.courtData[i]._id;
      }
    }
  }

  setClientId(data) {
    console.log("In Client")
    let id = data;
    console.log(id);
    let phoneNo = [] = id.split(' ');
    console.log(phoneNo[phoneNo.length - 1]);

    for (var i = 0; i < this.clientData.length; i++) {
      if (this.clientData[i].phoneNo === parseInt(phoneNo[phoneNo.length - 1])) {
        console.log(this.clientData[i]._id);
        this.caseForm.value.clientId = this.clientData[i]._id;
        console.log(this.clientId)
      }
    }

  }

  getClientData() {
    this.loading = true;
    this.http.simpleGet('client').subscribe((res: any) => {
      console.log("getting Client data", res);
      this.clientData = res.doc;
      for (let i = 0; i < this.clientData.length; i++) {
        this.clientNameAndPhoneNumberArray.push(this.clientData[i].name + " " + this.clientData[i].phoneNo)
      }
      console.log(this.clientNameAndPhoneNumberArray);
      this.loading = false;

    }, err => {
      console.log(err);

    })
  }

  getCourtData() {
    this.loading = true;

    this.http.simpleGet('court')
      .subscribe((res: any) => {
        console.log("Getting Court Data", res);
        this.courtData = res.doc;
        for (let i = 0; i < this.courtData.length; i++) {
          this.courtNameArray.push(this.courtData[i].courtName);
        }
        console.log(this.courtNameArray);

      }, err => {
        console.log(err);

      })
  }

  addOppositePartyNames(value) {

    this.oppositePartyNamesArray.push(value);
    console.log(value);
    console.log(this.oppositePartyNamesArray);
  }

  removeOppositePartyNames(value) {
    console.log(value);
    for (var i = 0; i < this.oppositePartyNamesArray.length; i++) {
      if (this.oppositePartyNamesArray[i] === value) {
        this.oppositePartyNamesArray.splice(i, 1);
        console.log(this.oppositePartyNamesArray);
      }
    }
  }



  async submit() {
    this.loading = true;

    this.submitted = true;
    if (this.caseForm.invalid) {
      this.toastr.error("Please Filled all fields");
      this.loading = false;
      return;
    }
    console.log(this.caseForm.value);
    this.caseForm.value.oppositeParty = this.oppositePartyNamesArray;
    await this.setClientId(this.caseForm.value.clientId);
    await this.setCourtId(this.caseForm.value.courtId);
    console.log(this.caseForm.value.courtId)
    this.caseForm.value.advocateRefId = this.token;
    console.log(this.caseForm.value);

    this.http.post('case', this.caseForm.value).subscribe(res => {
      console.log("Success", res);
      this.loading = false;
      this.toastr.success(res.message);
      this.caseForm.reset();

    },
      err => {
        console.log(err);
        this.loading = false;
        this.toastr.error(err.message)

      })

  }


}
