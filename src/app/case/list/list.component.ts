import { Component, OnInit, TemplateRef } from '@angular/core';
import { HttpService } from 'src/app/_shared/services/http.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder } from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  searchText;
  caseId;
  addDate; 
  minDate:Date;
  nextDate:any;
  loading=false;
  clientId;
  reviewForm:FormGroup;
  caseData: any;
  modalRef: BsModalRef;
  constructor(private http: HttpService,
    private modalService: BsModalService,
    private fb:FormBuilder,
    private toastr:ToastrService ) { }

  ngOnInit() {

    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate() + 1);
    this.getCaseDetails();
    this.reviewForm=this.fb.group({
      nextDate:[''],
      review:this.fb.group({
        date:[''],
        remark:['']
      })
    })

  }

  openModal(caseId,clientId,template: TemplateRef<any>) {
    this.caseId=caseId;
    console.log(this.caseId);
   
    this.clientId=clientId;
    console.log(this.clientId);
    this.modalRef = this.modalService.show(template,{class: 'modal-lg modal-dialog-centered', ignoreBackdropClick: true, keyboard: false});
    this.getOldDate();
  }

  getOldDate(){
    this.loading=true
    this.http.simpleGet('case/?id='+this.caseId)
    .subscribe((res:any)=>{
      console.log("Getting case data",res);
      this.nextDate=res.doc.nextDate;
      // this.reviewForm.value.review.date=
      console.log(this.reviewForm.value);
      this.loading=false
    },err=>{
      console.log(err)
    })
  }

  hideModal() {
    this.modalRef.hide()
  }

  getCaseDetails() {
    this.loading=true;
    this.http.simpleGet('case/?id='+localStorage.getItem('token')).subscribe((res: any) => {
    
      console.log("Getting Case Details", res);
      this.caseData = res.doc;
      console.log(this.caseData)
      this.loading=false;
    }, 
    err => {
      console.log(err)
    })
  }

  deleteCase(id) {
    console.log(id);
    Swal.fire({
      title: 'Error!',
      text: 'Do you want to continue',
      type: 'error',
      confirmButtonText: 'Cool'
    })
    // this.loading=true;
    // this.http.delete('case/' + id)
    //   .subscribe((res:any) => {
    //     console.log("Deleted Successfully", res);
    //     this.toastr.success(res.message)
    //     this.getCaseDetails();
    //     this.loading=false;
    //   }, err => {
    //     this.toastr.error(err.message)
    //     console.log(err)
  
    //   })
  }
  /*
{
  advocateEmailId:'',
  clientEmailId:'',
  nextdate:'',
  review:''

}
  */
  
  submit() {
    this.loading=true;
    let advocateEmailId;
   
    this.reviewForm.value.review.date=this.nextDate;
     console.log(this.reviewForm.value);
     console.log(this.caseData.advocateId,this.caseData.clientId);
     for(let i=0;i<this.caseData.length;i++)
     {
       if(this.caseData[i].caseId=this.caseId)
       {
          console.log(this.caseData[i].advocateId,this.caseData[i].clientId);
            advocateEmailId=this.caseData[i].advocateId.email;
          break;
       }
     }
  
    let updateObject= {
     advocateEmailId:advocateEmailId,
      clientEmailId:this.clientId.email,
      nextdate:this.reviewForm.value.nextDate,
      review:this.reviewForm.value.review
    
    }
    console.log(updateObject)
    this.http.put('case/review/'+this .caseId,updateObject).subscribe(res=>{
      console.log("Review Upadted",res);
      this.loading=false
    },err=>{
      console.log(err);
      this.loading=false

    })
    this.hideModal()

  }

 sendMail(){
   this.http.post("client/nextdate",this.clientId).subscribe(res=>{
     console.log("Mail Sent",res);
   },err=>{
     console.log(err)
   })
 }
}
