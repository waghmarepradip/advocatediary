import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { HttpService } from 'src/app/_shared/services/http.service';
import { flatten } from '@angular/compiler';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  selected: string;
  userForm: FormGroup;                            //  Case Form variable 
  courtId; 
  courtNameArray=[]                                       // variable to store the courtId
  permissionAaay: any = [];               // variable to store the names of the opposite party 
  courtData: any;     
  submitted:boolean=false;                        // variable to store the court data which helps in drop down of court name
  token: any;  
  loading=false;
  passwordMatch: boolean;

  constructor(private fb: FormBuilder,
    private http: HttpService) { }

  ngOnInit() {

    if (localStorage.length > 0) {
      // We have items
      console.log("Data found")                      // To check the local storage is empty or not

      this.token = localStorage.getItem("token");
      console.log(this.token);
    } else {
      // No items
      console.log("No data")
    }



      //  this.getCourtData()
    this.userForm = this.fb.group({
      phoneNo: ['',[ Validators.required,Validators.pattern('^[0-9]*$'),Validators.minLength(10),Validators.maxLength(10)]],
      name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      email: ['',  [Validators.required, Validators.email]],
      permissions: ['', Validators.required],
      password: ['', [Validators.required]],
      conformPassword: ['',[ Validators.required]],
      advocateRefId: ['']

  })


}

 // convenience getter for easy access to form fields Validators.compose([Validators.min(1000000000), Validators.max(9999999999)])
 get f() { return this.userForm.controls; }


//  selectCourtId(data) {
//   console.log(data);
//   this.setCourtId(data);
// }

// setCourtId(id){
//   console.log("sjkskskssk")
//  for(var i=0;i<this.courtData.length;i++)
//   {
//     if(this.courtData[i].courtName===id)
//     {
//       console.log(this.courtData[i]._id);
//       this.courtId=this.courtData[i]._id;
//     }
//    }

// }

// getCourtData() {
//   this.http.simpleGet('court')
//     .subscribe((res: any) => {
//       console.log("Getting Court Data", res);
//       this.courtData = res.doc;
//       for(let i=0;i<this.courtData.length;i++)
//       {
//         this.courtNameArray.push(this.courtData[i].courtName);
//       }
//       console.log(this.courtNameArray)
//     }, err => {
//       console.log(err);
//     })
// }

addOppositePartyNames(value) {

  this.permissionAaay.push(value);
  console.log(value);
  console.log(this.permissionAaay);
}

removeOppositePartyNames(value) {
  console.log(value);
  for (var i = 0; i < this.permissionAaay.length; i++) {
    if (this.permissionAaay[i] === value) {
      this.permissionAaay.splice(i, 1);
      console.log(this.permissionAaay);
    }
  }
}



submit() {
 
  this.submitted=true;
  console.log("form call");
 if(this.userForm.invalid)
 {
   console.log("CHECK FORM VALIDATION", this.userForm.invalid);
   return;
 }

 if(this.userForm.value.password === this.userForm.value.conformPassword ){
  this.loading=true;
  this.passwordMatch =false;
  console.log(this.userForm.value);
  this.userForm.value.permissions = this.permissionAaay;
  // this.userForm.value.courtId = this.courtId;
   this.userForm.value.advocateRefId = this.token;
  console.log(this.userForm.value);

  this.http.post('user', this.userForm.value).subscribe(res => {
    console.log("Success", res);
    this.loading=false;
  },
    err => {
      console.log(err);
      this.loading=false;
    })

  }
  else{
    this.passwordMatch =true;
    console.log(this.passwordMatch);
    }
}


}

