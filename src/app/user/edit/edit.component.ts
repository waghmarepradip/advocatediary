import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpService } from 'src/app/_shared/services/http.service';
import { Params, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  selected: string;
  userForm: FormGroup;                            //  Case Form variable 
  courtId; 
  courtNameArray=[]                                       // variable to store the courtId
  parmission
  permissionAaay: any = [];               // variable to store the names of the opposite party 
  userData: any;     
  submitted:boolean=false;                         // variable to store the court data which helps in drop down of court name
  token: any;  
  loading: boolean;
   id;
   passwordMatch: boolean = false;
  constructor(private fb: FormBuilder,
    private http: HttpService, private route: ActivatedRoute) { }

  ngOnInit() {

    if (localStorage.length > 0) {
      // We have items
      console.log("Data found")                      // To check the local storage is empty or not

      this.token = localStorage.getItem("token");
      console.log(this.token);
    } else {
      // No items
      console.log("No data")
    }


    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];                               // To get Id from active routes
      console.log("Constructor==>", this.id)
    });


      this.getUserData()
    this.userForm = this.fb.group({
      phoneNo: ['',[ Validators.required,Validators.pattern('^[0-9]*$'),Validators.maxLength(10),Validators.minLength(10)]],
      name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      email: ['',  [Validators.required, Validators.email]],
      permissions: ['', Validators.required],
      password: ['',],
      newPassword: ['',],
      conformPassword: ['',],
      advocateRefId: ['']

  })


}

getUserData(){
  this.loading=true
  this.http.simpleGet('user/?id='+this.id) //'user/?id='+id
  .subscribe((res:any)=>{
    this.loading=false;
    console.log("Getting court Data",res);
    this.userData=res.doc;
    console.log(this.userData);
    this.patchValuToForm();

  },err=>{
    console.log(err);
  })
}

patchValuToForm(){

  this.permissionAaay = this.userData.permissions;
  this.userForm.patchValue(this.userData);
}


 // convenience getter for easy access to form fields
 get f() { return this.userForm.controls; }




addOppositePartyNames(value) {

  this.permissionAaay.push(value);
  console.log(value);
  console.log(this.permissionAaay);
}

removeOppositePartyNames(value) {
  console.log(value);
  for (var i = 0; i < this.permissionAaay.length; i++) {
    if (this.permissionAaay[i] === value) {
      this.permissionAaay.splice(i, 1);
      console.log(this.permissionAaay);
    }
  }
}



submit() {
  this.submitted=true;
  // this.loading=true;
 if(this.userForm.invalid)
 {
   return;
 }
 if(this.userForm.value.newPassword === this.userForm.value.conformPassword )
 {
  this.loading=true;
  this.passwordMatch =false;

  console.log(this.userForm.value);
  this.userForm.value.permissions = this.permissionAaay;
  // this.userForm.value.courtId = this.courtId;
  this.userForm.value.advocateRefId = this.token;
  console.log(this.userForm.value);

  this.http.put('user/?id='+this.id, this.userForm.value).subscribe(res => {
    console.log("Success", res);
    this.loading=false;
  },
    err => {
      console.log(err);
      this.loading=false;
    })

 
}
else{
this.passwordMatch =true;
console.log(this.passwordMatch);
}

}

}


