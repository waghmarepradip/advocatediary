import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/_shared/services/http.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  searchText;
  loading=false;
  userData: any;

  constructor(private http: HttpService) { }

  ngOnInit() {
    this.getUserDetails()
  }

  getUserDetails() {
    this.http.simpleGet('user').subscribe((res: any) => {
      console.log("Getting User Details", res);
      this.userData = res.doc;
      this.loading=false;

    }, err => {
      console.log(err)
    })

  }

  deleteUser(id) {
    this.loading=true;
    console.log(id);
    this.http.delete("user/?id="+id)
      .subscribe(res => {
        console.log("Deleted Successfully", res);
        this.getUserDetails();
        this.loading=false;
      }, err => {
        console.log(err)
      })

  }

  edit(data){
    console.log("edit data" , data);
  }
}

