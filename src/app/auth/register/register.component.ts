import { Component, OnInit } from '@angular/core';
import { GoogleLoginProvider, LinkedinLoginProvider, FacebookLoginProvider, AuthService } from 'ng4-social-login';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpService } from 'src/app/_shared/services/http.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  fileToUpload: File = null;    // For File Upload
  selectedFile: File = null;    // For Selection of file
  user: any                     // User variable for social media login to store user data
  public loading = false;       // For loading animation
  submitted = false;            // for Form Validation

  fd = new FormData();
  constructor(private fb: FormBuilder,
    private toastr: ToastrService,
    private socialAuthService: AuthService,
    private http: HttpService) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      email: ['', [Validators.required, Validators.email]],
      password: ['',[Validators.required, Validators.pattern('((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,30})'), Validators.minLength(6)]],
      phone: ['', [Validators.required, Validators.pattern('[0-9]+'),Validators.minLength(10),Validators.maxLength(10)]]
    })
  }

  // convenience getter for easy access to form fields
  get validate() { return this.registerForm.controls; }



  async onSubmit() {
    this.submitted = true;                                     // Register method for registeration of new user
    this.loading = true;
    if (this.registerForm.invalid) {

      console.log("Invalid", this.registerForm.value)
      this.toastr.error("Fill all required fields");
      this.loading = false;
      return;

    }

    console.log(this.registerForm.value);
    var formData = this.registerForm.value;

    var EMP = {
      'name': formData.name,
      'email': formData.email,
      'password': formData.password,
      'phoneNo': formData.phone

    };

    console.log(this.registerForm.value)
    this.http.post('register', EMP)
      .subscribe((res: any) => {
        console.log("SuccessFull", res);
        this.toastr.success(res.message);
        this.registerForm.reset()
        this.loading = false;
      },
        err => {
          console.log(err)
          this.toastr.error(err.message);

        }
      )

    this.loading = false;

  }
  onFileSelected(event) {
    console.log(event);
    this.selectedFile = <File>event.target.files[0];
    this.fd.append('image', this.selectedFile);
    console.log(event.target.files);
    console.log(this.selectedFile.name);

  }

  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);
    var reader = new FileReader();
    reader.onload = (event: any) => {
    }
    reader.readAsDataURL(this.fileToUpload);
  }

  googleLogin() {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then((userData) => {
      this.user = userData
      console.log(this.user);
      this.http.post('register', this.user)
        .subscribe(res => {
          console.log("SuccessFull", res);
          this.toastr.success('Registration Successfull');
          this.registerForm.reset()
          this.loading = false;
        },
          err => console.log(err))
      this.loading = false;


    });
  }

  linkdinLogin() {
    this.socialAuthService.signIn(LinkedinLoginProvider.PROVIDER_ID).then((userData) => {
      this.user = userData
    });

  }

  facebookLogin() {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then((userData) => {
      this.user = userData
      console.log(this.user);
      this.http.post('register', this.user)
        .subscribe(res => {
          console.log("SuccessFull", res);
          this.toastr.success('Registration Successfull');
          this.registerForm.reset()
          this.loading = false;
        },
          err => console.log(err))
      this.loading = false;

    });

  }

}
