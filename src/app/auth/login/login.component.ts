import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/_shared/services/http.service';
import { AuthService, LinkedinLoginProvider, GoogleLoginProvider, FacebookLoginProvider } from 'ng4-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  user
  usersArray: any = []
  photoUrl;
  submitted = false;            // for Form Validation


  public loading = false;
  constructor(private modalService: BsModalService,
    private fb: FormBuilder, private toastr: ToastrService,
    private socialAuthService: AuthService,
    private http: HttpService,
    private route: Router) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    })
  }


  // convenience getter for easy access to form fields
  get validate() { return this.loginForm.controls; }


  login() {

    this.submitted = true;                                     // Register method for registeration of new user
    this.loading = true;
    if (this.loginForm.invalid) {
      console.log("Invalid", this.loginForm.value)
      this.toastr.error("Fill all required fields");
      this.loading = false;
      return;
    }

    console.log(this.loginForm.value)
    this.http.post('login/checklogin', this.loginForm.value).subscribe(
      (res: any) => {
        console.log("Login Successfull", res)



        if(res.doc){
          let token = res.doc;
          localStorage.setItem('token', token);
        }

        let token = res.doc;
        let profile:any={
          name :res.name,
          profiePic : res.profiePic,
          status:res.status
        }
        localStorage.setItem('profile', JSON.stringify(profile))
      
        this.loading = false;
        this.route.navigate(['dashboard']);
        this.toastr.success('Login Successfull');
      }
      , err => {
        console.log(err)
        this.loading = false;
        this.toastr.error(err.error.message);
      });
  }

  getUsers() {
    this.http.simpleGet('register').subscribe(res => {
      console.log("Getting Users", res)
      this.usersArray = res

    })
  }

  googleLogin() {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then((userData) => {
      this.user = userData
      console.log(this.user)
      this.photoUrl = this.user.photoUrl
    });
  }

  linkdinLogin() {
    this.socialAuthService.signIn(LinkedinLoginProvider.PROVIDER_ID).then((userData) => {
      this.user = userData
    });

  }

  facebookLogin() {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then((userData) => {
      this.user = userData
      console.log(this.user)
    });

  }



  register() {
    console.log("login call");
    this.route.navigate(['home/register']);
  }


}
