import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService, SocialUser, GoogleLoginProvider, FacebookLoginProvider, LinkedinLoginProvider } from 'ng4-social-login'
import { ToastrService } from 'ngx-toastr';
import { HttpService } from '../../_shared/services/http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
 salientFeature : any = [];
 features : any = [];
 contact : any =[];
 price : any = [];
  constructor(
    private toastr: ToastrService,
    private route: Router,
  ) {

  }


  ngOnInit() {
   this.salientFeature.push({feature : 'Create New Case by automatically fetching case details from CNR'});
   this.salientFeature.push({feature : 'Complete History of Case Hearing with all previous dates.'});
   this.salientFeature.push({feature : 'Automatically fetch Next Hearing Date'});
   this.salientFeature.push({feature : 'Send SMS alerts to clients.'});
   this.salientFeature.push({feature : 'All Data is kept confidential and fully secured.'});
   this.salientFeature.push({feature : 'Automatic data backup on a daily basis.'});
   console.log("salientFeature",this.salientFeature); 

   this.features.push({heading : 'My Case',text : 'Todays Tomorrows & Date Awaited Cases',class :'fab fa-black-tie'});
   this.features.push({heading : 'Case History',text : 'Complete Case History With All Previous Case Dates!',class :'fas fa-align-justify'});
   this.features.push({heading : 'Customizable',text : 'Customized Fields like Case Type, Court, U/S.',class :'fas fa-address-card'});
   this.features.push({heading : 'Reports',text : 'Generate & Print Reports using Case No., Ref. No., Dat',class :'fas fa-book-open'})
   this.features.push({heading : 'Client Details',text : 'Keep client details handy and easily accessible.',class :'far fa-address-book'});
   this.features.push({heading : 'Case Fees',text : 'No need to remember case fees. Keep it private and secure.',class :'fab fa-black-tie'})
   this.features.push({heading : 'Opp. Council Details',text : 'Access opposite council details and get in touch quickly.',class :'fas fa-balance-scale'});
   this.features.push({heading : 'Payments',text : 'Tracks the case payments and show any pending payments.',class :'fab fa-amazon-pay'})
  console.log(this.features,"feature")

  this.contact.push({heading : 'Contact Number',text : '123456789',class :'fas fa-phone-alt'});
  this.contact.push({heading: 'Email',text : 'asdfsafsadasfdsad@gmail.com', class : 'fas fa-envelope-square'});
  this.contact.push({heading:'Office', text:'baner road balewadi tq .dist pune' ,class:'fas fa-clock'})
  console.log(this.contact,"contact")
  
  this.price.push({ cost :'50'});
  this.price.push({ cost :'70'});
  console.log("cost" , this.price);
  

}




login(){
console.log("login call");
this.route.navigate(['home/login']);
}

register(){
  console.log("login call");
  this.route.navigate(['home/register']);
  }
  
}
