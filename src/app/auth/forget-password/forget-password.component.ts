import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from 'src/app/_shared/services/http.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

  forgotPasswordForm: FormGroup;
  constructor(private fb: FormBuilder,
              private http:HttpService) { }

  ngOnInit() {
    this.forgotPasswordForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(/^(\d{10}|\w+@\w+\.\w{2,3})$/)])],
    })
  }

  submit() {
    if (this.forgotPasswordForm.valid) {
      console.log(this.forgotPasswordForm.value)
      this.http.post('register/forgot',this.forgotPasswordForm.value)
      .subscribe(res=>{console.log("Successfull",res)},
      err=>{
        console.log(err)
      }) 
    } else {
      console.log("Invalid Email")
    } 

  }

}
