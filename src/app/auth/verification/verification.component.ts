import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { HttpService } from 'src/app/_shared/services/http.service';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.css']
})
export class VerificationComponent implements OnInit {
id:any;
public loading = false;       // For loading animation

  constructor(private http:HttpService,
              private route:ActivatedRoute) { }

  ngOnInit() {

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      console.log("Constructor==>", this.id)
    })
    this.verification();

  }

  verification(){
    this.loading=true;
    this.http.put("register/verify/?id="+this.id)
    .subscribe(res=>{
      console.log(res);
      this.loading=false;
      
    },err=>{
      console.log(err)
    })
  }



}
