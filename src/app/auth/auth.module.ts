import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider, LinkedinLoginProvider} from 'ng4-social-login'
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { ErrorPageComponent } from './error-page/error-page.component'
import { HomeComponent } from './home/home.component';
import { VerificationComponent } from './verification/verification.component';
import { ToastrModule} from 'ngx-toastr';


const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('361356650655-juoimtd8q9gmha7u0ae2lehfmndt0109.apps.googleusercontent.com')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('648819102289874')
  },
  {
    id: LinkedinLoginProvider.PROVIDER_ID,
    provider: new LinkedinLoginProvider('81nxdovy5i2whz')
  }

], false)

export function provideConfig() {
  return config;
}



@NgModule({
  declarations: [LoginComponent, RegisterComponent, ForgetPasswordComponent, ResetPasswordComponent,ErrorPageComponent,HomeComponent,VerificationComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule,  
    NgxLoadingModule,
    ShowHidePasswordModule,
    SocialLoginModule,
    ToastrModule.forRoot() // ToastrModule added
  ]
})
export class AuthModule { }
