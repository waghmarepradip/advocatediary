import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ErrorPageComponent } from './error-page/error-page.component'
import { HomeComponent } from './home/home.component';
import { VerificationComponent } from './verification/verification.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'forgotPassword', component: ForgetPasswordComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'reset/:id', component: ResetPasswordComponent },
  { path: 'error', component: ErrorPageComponent },
  // {path: 'home',component:HomeComponent} 
  { path: 'verification/:id', component: VerificationComponent},


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
