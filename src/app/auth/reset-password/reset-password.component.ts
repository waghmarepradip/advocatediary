import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpService } from 'src/app/_shared/services/http.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  resetPasswordForm: FormGroup;
  id;
  constructor(private fb: FormBuilder,
    private toastr: ToastrService,
    private http: HttpService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.resetPasswordForm = this.fb.group({
      password: ['', Validators.required],
      confirmedPassword: ['', Validators.required]
    })

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      console.log("Constructor==>", this.id);
      // localStorage.setItem('token',this.id)
    })
  }

  submit() {
    if (this.resetPasswordForm.valid) {
      console.log(this.resetPasswordForm.value.password)
      console.log(this.resetPasswordForm.value.confirmedPassword)
      if (this.resetPasswordForm.value.password === this.resetPasswordForm.value.confirmedPassword) {
       var data={
         password:this.resetPasswordForm.value.password
       }
        this.http.put('register/resetpass/?id=' + this.id, data)
          .subscribe(res => {
            console.log("Success", res)
          },
            err => {
              console.log(err)
            })
        this.toastr.success("Reset Password Successfull")
      }
      else {
        console.log("Password Not Match")
      }
    }
    else {
      console.log("Invalid")
    }


  }
}
