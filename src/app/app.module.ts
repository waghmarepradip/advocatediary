import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthlayoutComponent } from './_shared/layout/authlayout/authlayout.component';
import { CommonlayoutComponent } from './_shared/layout/commonlayout/commonlayout.component';
import { HeaderComponent } from './_shared/layout/header/header.component';
import { FooterComponent } from './_shared/layout/footer/footer.component';
import { NavbarComponent } from './_shared/layout/navbar/navbar.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthRoutingModule } from './auth/auth-routing.module'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { ModalModule } from 'ngx-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AvatarModule } from 'ngx-avatar';
// import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider, LinkedinLoginProvider } from 'ng4-social-login'
// import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { NgxLoadingModule } from 'ngx-loading';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ToastrModule } from 'ngx-toastr';
import { TagInputModule } from 'ngx-chips';
import { CaseModule } from './case/case.module';
import { AdvocateModule } from './advocate/advocate.module';
import { CourtModule } from './court/court.module';
import { AuthModule } from './auth/auth.module';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider, LinkedinLoginProvider, SocialLoginModule } from 'ng4-social-login';
import { CustomerModule } from './customer/customer.module';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { UsersModule } from './user/users.module';
import { AuthGuard } from './_shared/services/auth.guard';
import { InterceptorService } from './_shared/services/interceptor.service';
import {MailverifyComponent } from './_shared/layout/mailverify/mailverify.component';

// import { SweetAlertService } from 'ngx-sweetalert2';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('361356650655-juoimtd8q9gmha7u0ae2lehfmndt0109.apps.googleusercontent.com')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('648819102289874')
  },
  {
    id: LinkedinLoginProvider.PROVIDER_ID,
    provider: new LinkedinLoginProvider('81nxdovy5i2whz')
  }

], false)

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    AuthlayoutComponent,
    CommonlayoutComponent,
    HeaderComponent,
    FooterComponent,
    NavbarComponent,
    MailverifyComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AuthRoutingModule,
    BrowserAnimationsModule, // required animations module
    // ModalModule.forRoot(),
    ReactiveFormsModule,
    AvatarModule,
    HttpClientModule,
    SocialLoginModule,
    FormsModule,
    TagInputModule,
    ShowHidePasswordModule,
    // ShowHidePasswordModule,
    NgxLoadingModule.forRoot({}),
    Ng2SearchPipeModule,
    // SweetAlertService,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-top-right',
      closeButton:true,
      progressBar:true,
      preventDuplicates: true,
    }
    ),
    CaseModule,
    AdvocateModule,
    CourtModule,
    AuthModule,
    CustomerModule,
    UsersModule,
    TypeaheadModule.forRoot(),
  ],
  providers: [{
    provide: AuthServiceConfig,
    useFactory: provideConfig,
    
  },
  AuthGuard,
  {
    provide:HTTP_INTERCEPTORS,
    useClass : InterceptorService,
    multi : true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
