import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpService } from 'src/app/_shared/services/http.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  advocateForm: FormGroup;
  skills: any = [];
  public loading = false;
  advocateFormData = new FormData();
  skillsError: boolean;
  submitted: boolean;
  constructor(private http: HttpService,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.advocateForm = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      officeAddress: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phoneNo: ['', [Validators.required, Validators.pattern('[0-9]+')]],
      advocateskills: ['', [Validators.required]]
    })
  }

  // convenience getter for easy access to form fields
  get f() { return this.advocateForm.controls; }



  addSkills(value) {

    this.skills.push(value);
    console.log(value);
    console.log(this.skills);
    if (this.skills.length > 1 || this.skills.length === 0)
      this.skillsError = false;
    else
      this.skillsError = true;

  }

  removeSkills(value) {
    console.log(value);
    for (var i = 0; i < this.skills.length; i++) {
      if (this.skills[i] === value) {
        this.skills.splice(i, 1);
        console.log(this.skills);
      }
    }
    if (this.skills.length > 1 || this.skills.length === 0)
      this.skillsError = false;
    else
      this.skillsError = true;

  }


  submit() {

    this.submitted = true;                                     // Register method for registeration of new user
    this.loading = true;
    if (this.advocateForm.invalid) {

      console.log("Invalid", this.advocateForm.value)
      // this.toastr.error("Fill all required fields");
      console.log("All field required.")
      this.loading = false;
      return;
    }
    this.advocateForm.value.advocateskills = this.skills;
    console.log(this.advocateForm.value)
    this.http.post('advocatedetail', this.advocateForm.value)
      .subscribe(res => {
        console.log("SuccessFull", res)
        this.loading = false
      }, err => {
        console.log(err.error.text)
        if (err.error.text === "email Already Present.") {
          console.log("Done")
        }
        this.loading = false
      })


  }

}
