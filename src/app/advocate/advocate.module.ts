import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdvocateRoutingModule } from './advocate-routing.module';
import { ViewComponent } from './view/view.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TagInputModule } from 'ngx-chips';
import { NgxLoadingModule } from 'ngx-loading';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ToastrModule } from 'ngx-toastr';


@NgModule({
  declarations: [ViewComponent, AddComponent, EditComponent, ListComponent],
  imports: [
    CommonModule,
    AdvocateRoutingModule,
    ReactiveFormsModule,
    TagInputModule,
    FormsModule,
    NgxLoadingModule.forRoot({}),
    ToastrModule.forRoot(),

    Ng2SearchPipeModule
  ]
})
export class AdvocateModule { }
