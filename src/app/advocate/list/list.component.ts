import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/_shared/services/http.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  advocateData: any;
  searchText;
  public loading = false;

  constructor(private http: HttpService) { }

  ngOnInit() {
    this.getAdvocateData()
  }

  getAdvocateData() {
    this.loading = true
    this.http.simpleGet('advocatedetail')
      .subscribe((res: any) => {
        this.loading = false;
        console.log("Getting Advocate Data", res)
        this.advocateData = res.doc;
        console.log(this.advocateData)

      }, err => {
        console.log(err)
      })
  }

  deleteAdvocateRecord(id) {
    this.loading = true;
    this.http.delete('advocatedetail/?id=' + id)
      .subscribe(res => {
        console.log("Record Deleted Successfully", res);
        this.getAdvocateData();
        this.loading = false;
      }, err => {
        console.log(err);
      })

  }

}
