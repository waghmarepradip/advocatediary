import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpService } from 'src/app/_shared/services/http.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  advocateForm: FormGroup;
  id;
  private loading = false;
  skills: any;
  submited: boolean = false;
  skillsError;
  advocateData: any;
  constructor(private fb: FormBuilder,
    private http: HttpService,
    private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      console.log("Constructor==>", this.id)
    })

    this.getAdvocateData();


    this.advocateForm = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      officeAddress: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phoneNo: ['', [Validators.required, Validators.pattern('[0-9]+')]],
      advocateskills: ['', [Validators.required, Validators.minLength(2)]]
    })

  }

  getAdvocateData() {
    this.loading = true
    this.http.simpleGet('advocatedetail/?id=' + this.id)
      .subscribe((res: any) => {
        this.loading = false;
        console.log("Getting Advocate Data", res);
        this.advocateData = res.doc;
        console.log(this.advocateData);
        this.patchValuToForm();

      }, err => {
        console.log(err);
      })

  }

  // convenience getter for easy access to form fields
  get f() { return this.advocateForm.controls; }


  patchValuToForm() {
    this.advocateForm.patchValue(this.advocateData);
    this.skills = this.advocateData.advocateskills;

  }

  addSkills(value) {
    this.skills.push(value);
    console.log(value);
    console.log(this.skills);
    if (this.skills.length > 1 || this.skills.length === 0)
      this.skillsError = false;
    else
      this.skillsError = true;

  }

  removeSkills(value) {
    console.log(value);
    for (var i = 0; i < this.skills.length; i++) {
      if (this.skills[i] === value) {
        this.skills.splice(i, 1);
        console.log(this.skills);
      }
    }
    if (this.advocateForm.value.advocateskills
      .length > 1 || this.skills.length === 0) {
      this.skillsError = false;
      console.log("false");
    }
    else
      this.skillsError = true;

  }

  submit() {
    this.submited = true
    if (this.advocateForm.valid) {
      this.advocateForm.value.advocateskills = this.skills;
      this.http.put('advocatedetail/?id=' + this.id, this.advocateForm.value)
        .subscribe(res => {
          console.log("Successfull", res);
        },
          err => {
            console.log(err);
          })
    } else {
      console.log("All Fields required");
      return;
    }
  }

}
