import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_shared/services/auth.guard';
import { AuthlayoutComponent } from './_shared/layout/authlayout/authlayout.component';
import { ErrorPageComponent } from './auth/error-page/error-page.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },


{
  path : '',
  children:[
    {
      path : 'home', 
      loadChildren : './auth/auth.module#AuthModule'
    }
  ]

},

  {
    path: '',
    component: AuthlayoutComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'case',
        loadChildren: './case/case.module#CaseModule'
      },
      {
        path: 'court',
        loadChildren: './court/court.module#CourtModule'
      },
      {
        path: 'advocate',
        loadChildren: './advocate/advocate.module#AdvocateModule'
      },
      {
        path: 'client',
        loadChildren: './customer/customer.module#CustomerModule'
      },
      {
        path: 'user',
        loadChildren: './user/users.module#UsersModule'
      },
    ],
    canActivate:[AuthGuard]
  },
  {
    path: '**',
    component: ErrorPageComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
