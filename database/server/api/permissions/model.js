const mongoose=require('mongoose');
const Schema=mongoose.Schema;

const permission=new Schema({
    category:{
        type:String,
     
    },
    subcategory:[{
        type:String,
     
    }],
    flag:{
        type:Boolean,
        default:true
    }
})
module.exports=mongoose.model('permission',permission);