const router=require('express').Router();
const controller=require('./controller');

async function get(req,res){
    console.log('Get');
    let response=await controller.getRecord(req,res);
    res.status(response.status).json({message: response.message});
}
async function post(req,res){
    console.log('post');
    let response=await controller.createCategory(req,res);
    res.status(response.status).json({message: response.message});
}

async function put(req,res){
    console.log('Put');
    let response=await controller.updateCategory(req,res);
    res.status(response.status).json({message: response.message});

}
async function deleteOne(req,res){
    console.log('Delete');
    let response=await controller.deleteOne(req,res);
    res.status(response.status).json({message: response.message});
}
router.get('/?',get);
router.post('/',post);
router.put('/?',put);
router.delete('/?',deleteOne);
module.exports=router;