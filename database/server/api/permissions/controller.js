const permissionModel=require('./model');
const get=require('lodash/get');

async function getRecord(req,res){
    try{
    console.log('Get Records',get(req,'query.id',''));
    let record=req.query.id?
                await permissionModel.findOne({'_id':req.query.id ,'flag':true}):
                await permissionModel.find({'flag':true});
    if(!record){
        throw new Error('Record Not Found');
    }
    console.log(record);
    return res.status(200).json({doc:record});
}catch(error){
    return{
        status:500,
        message:'Internal Server Error.'
    }

}

}
async function createCategory(req,res){
    try{
   console.log('Create Ctegory',get(req,'body',''));
   let subcategory=[];
   for(let i=0;i<req.body.subcategory.length;i++){
    subcategory[i]=req.body.subcategory[i]
    
}
console.log('Subcategory',subcategory);
   let data=new permissionModel();
   data.category=get(req,'body.category','');
   data.subcategory=subcategory;
   await data.save();
   return{
        status:200,
        message:'Success.'
   }
    }catch(error){
        return{
            status:500,
            message:'Internal Server.'
        }
    }

}
async function updateCategory(req,res){
    try{
        console.log('Update Category.',get(req,'query.id',''),get(req,'body',''));
        let subcategory=[];
        for(let i=0;i<req.body.subcategory.length;i++){
            subcategory[i]=req.body.subcategory[i];
        }
        console.log('subcategory:',subcategory);
        let data= await permissionModel.findOne({'_id':req.query.id ,'flag':true});
        if(!data){
            return res.status(404).json({message : 'data Not Found.'});
        }
        console.log('Before Update',data);
        data.category=get(req,'body.category','');
        data.subcategory=subcategory;
        console.log('data:',data);
        await data.save();
        
        return{
            status:200,
            message:'Updates Successful.'
        }
    }catch(error){
        return{
            status:500,
            message:'Internal Server Error.'
        }
    }
}
async function deleteOne(req,res){
    try{
      console.log('Delete record:',get(req,'query.id',''));
      await permissionModel.updateOne({'_id':req.query.id },{$set:{'flag':false}});
      return{
          status:200,
          message:'Document Delete Successful'
      }
    }catch(error){
        return{
            status:500,
            message:'Internal Server Error.'
        }
    }
}
module.exports={
    getRecord,
    createCategory,
    updateCategory,
    deleteOne
    
}

