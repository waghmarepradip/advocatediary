const userModel = require('./model');
const caseModel = require('../casemgt/model');
const get = require('lodash/get');
const jwt = require('jsonwebtoken');
const{ config}=require('../../core');
const bcrypt = require('bcrypt');
async function create(req, res) {
    try {
        console.log('Advocate Deatils', get(req, 'body', ''), "file is:", get(req, 'file', ''));
        // let decode=jwt.verify(req.body.advocateRefId,config.secretkey);
        // console.log('decoed',decode.subject);
        let permission=[];
        for(let i=0;i<req.body.permissions.length;i++){
            permission[i]=req.body.permissions[i];
        }
        console.log('Permissions=>',permission)
        let userData = new userModel();
        userData.name = get(req, 'body.name', '');
        userData.phoneNo = get(req, 'body.phoneNo', '');
        userData.email = get(req, 'body.email', '');
        userData.permissions=permission;
        let hash = bcrypt.hashSync(get(req, 'body.password', ''), 10);
        userData.password = hash;
        userData.advocateId = req.decoded.subject;

        userData.image = get(req, 'file.filename', '');

        let record = await userModel.findOne({ 'email': get(req, 'body.email', '') });
        if (record) {
            return res.send('Email Already Present.');
        }
        let saveResponse = await userData.save();
        if (!saveResponse) {
            throw new Error('Document Not Save.')
        }

        return res.status(200).json({ message: 'document successfully inserted.' });

    } catch (error) {
        console.log("Error Occur in create function", error);
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}

async function getRecord(req, res) {
    try {
        console.log('All Records', req);
        const record = req.query.id ?
            await userModel.findOne({ _id: req.query.id, 'flag': true }, { flag: 0 }) :
            await userModel.find({ flag: 'true' }, { flag: 0 });

        if (!record) {
            return Promise.reject('Record Not Found.');
        }
        return res.status(200).json({ status: 200, message: 'success', doc: record });

    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}

async function update(req, res) {
    try {
        console.log('Update Record', get(req, 'body', ''), 'Id', req.query.id);
        let response = await userModel.findOne({ '_id': req.query.id, 'flag': true });

        if (!response) {
            return res.send('Record Not Found.');
        }
      
        let permission=[];
        for(let i=0;i<req.body.permissions.length;i++){
            permission[i]=req.body.permissions[i];
        }
        console.log('Permissions=>',permission);
       
        let decode=jwt.verify(req.body.advocateRefId,config.secretkey);
        console.log('decode',decode.subject);
        console.log('new pass:',get(req,'body.newPassword',''));
        if(get(req,'body.newPassword','')){
            let hash = bcrypt.hashSync(get(req, 'body.newPassword', ''),10);
              response.password = hash;
        }
       else{
        response.password=get(req,'body.password','');
       }
        response.name = get(req, 'body.name', '');
        response.phoneNo = get(req, 'body.phoneNo', '');
        response.email = get(req, 'body.email', '');
        response.permissions=permission;
       
        response.advocateId = decode.subject;
        console.log('Respondse=>',response.name);
        console.log('Res===',response);
       await response.save();
       res.status(200).json({ message: 'document successfully updated.' });
       
        return {
            status: 200,
            message: 'success.'
        };
    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }

}
async function deleteOne(req, res) {
    try {
        console.log('Delete Record', req.query.id);
        // let response = await caseModel.updateOne({ 'advocateRefId': req.query.id }, { $set: { 'flag': false } });
        // if (!response) {
        //     throw new Error('case not found');
        // }
        await userModel.updateOne({ '_id': req.query.id }, { $set: { 'flag': false } });

        return res.status(200).json({ message: 'document successfully deleted.' });


    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}

async function checkLogin(req, res) {
    try {
        console.log('Check Login', get(req, 'body', ''));
        let record = await userModel.findOne({ 'email': get(req, 'body.email', ''), 'flag': true });
        if (!record) {
            return res.status(404).json({ message: 'Email Not Present' });
        }
        console.log('Record', record);
        let response = await bcrypt.compare(get(req, 'body.password', ''), record.password);
        if (response === false) {
            return res.status(404).json({ message: 'Password Not Match' });
        }
        let payload = { subject: record.id };
        let token = jwt.sign(payload, config.secretkey);
        return res.status(200).json({ doc: token, message: 'Login Successful.' });

    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}
module.exports = {
    create,
    getRecord,
    update,
    deleteOne,
    checkLogin
}
