const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autopopulateVar=require('mongoose-autopopulate');
var userSchema = new Schema({
    name: String,
    phoneNo: Number,
    email: {
        type: String,
        lowercase:true,
        unique:true
    },
  password:String,
  advocateId:{
   type:Schema.Types.ObjectId,
   ref:"advocateSchema",
   autopopulate:{
    select: 'name -_id' // remove listed fields from selection
  }
  },
  permissions:[{
   type:String
  }],
    flag: {
        type: Boolean,
        default: true
    },
    image: String,
    // permssion: {type: Number, required: true, default: 2}

});
userSchema.plugin(autopopulateVar);
module.exports = mongoose.model('userSchema', userSchema);