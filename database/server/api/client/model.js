const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const clientSchema = new Schema({
    name: String,
    phoneNo: Number,
    email: {
        type: String,
        required: true
    },
    address: String,
   
    flag: {
        type: Boolean,
        default: true
    }
});

module.exports = mongoose.model('clientSchema', clientSchema);