const clientModel = require('./model');
const get = require('lodash/get');
const XLSX=require('xlsx');
const casemodel=require('../casemgt/model');
const { mailService } = require('../../helper');
const { emailTemplateProvider } = require('../../helper');

async function createUser(req, res) {
    try {
        console.log('Create New client:', get(req, 'body', ''));
        let clientData = new clientModel();
        clientData.name = get(req, 'body.name', '');
        clientData.phoneNo = get(req, 'body.phoneNo', '');
        clientData.email = get(req, 'body.email', '');
        clientData.address = get(req, 'body.address', '');
        console.log('client data', clientData);
        let record = await clientModel.findOne({ 'email': clientData.email });
        if (record) {
            return res.status(200).json({ message: 'Email Already Present.' });
        }

        await clientData.save();
        return {
            status: 200,
            message: 'New Client Create Successful.'
        }


    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}

async function getUserRecord(req, res) {
    try {
        console.log('Client Record', get(req, 'query.id', ''));
        let response = req.query.id ?
            await clientModel.findOne({ '_id': req.query.id, 'flag': true }, { flag: 0 }) :
            await clientModel.find({ 'flag': true }, { flag: 0 });
        return res.status(200).json({ doc: response });


    } catch (error) {
        return {
            status: 500,
            message: 'internal Server Error'
        }
    }
}

async function UpdateUser(req, res) {
    try {
        console.log('Edit User', get(req, 'body', ''), 'id=>', get(req, 'query.id', ''));
        let record = await clientModel.findById({ '_id': get(req, 'query.id', ''), 'flag': true });
        if (!record) {
            throw new Error('record Not Found');
        }
        console.log('Befor Update', record);
        record.name = get(req, 'body.name', '');
        record.phoneNo = get(req, 'body.phoneNo', '');
        record.email = get(req, 'body.email', '');
        record.address = get(req, 'body.address', '');
        console.log('After Update', record);
        await record.save();
        return {
            status: 200,
            message: 'Record Update Successful.'
        }

    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}
async function deleteOneRecord(req, res) {
    try {
        console.log('Delete User Record', get(req, 'query.id', ''));
        await clientModel.updateOne({ '_id': req.query.id }, { $set: { 'flag': false } });
        return {
            status: 200,
            message: 'Record Delete Successful.'
        }

    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}
async function addManyClient(req,res){
    try{
        console.log('Client Record Data:',get(req,'file',''));
        console.log('====>',req);
        let workbook = XLSX.readFile(req.file.path);
        let sheet_name_list = workbook.SheetNames;
        let data=XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list]);
        console.log('Sheet List',data);

        await clientModel.insertMany(data);
        
        return{
            status:200,
            message:'Success.'
        }


    }catch(error){
        return{
            status:500,
            message:'Internal Server Error.'
        }
    }
}


module.exports = {
    createUser,
    getUserRecord,
    UpdateUser,
    deleteOneRecord,
    addManyClient,
  
}