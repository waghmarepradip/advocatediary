const router = require('express').Router();
const controller = require('./controller');
const fileUpload = require('../../helper').fileUpload;

async function post(req, res) {
    console.log('Post');
    let response = await controller.createUser(req, res);
    res.status(response.status).json({ message: response.message });
}
async function get(req, res) {
    console.log('Get');
    let response = await controller.getUserRecord(req, res);
    res.status(response.status).json({ message: response.message });
}
async function put(req, res) {
    console.log('Put');
    let response = await controller.UpdateUser(req, res);
    res.status(response.status).json({ message: response.message });
}
async function deleteOneRecord(req, res) {
    console.log('Delete');
    let response = await controller.deleteOneRecord(req, res);
    res.status(response.status).json({ message: response.message });
}
async function postclient(req,res){
    console.log('Add Many Client Record');
    let response= await controller.addManyClient(req,res);
    res.status(response.status).json({message: response.message});
}

router.post('/', post);
router.get('/?', get);
router.put('/?', put);
router.delete('/?', deleteOneRecord);

router.post('/bulkdata',fileUpload.single('file'),postclient);
module.exports = router;
