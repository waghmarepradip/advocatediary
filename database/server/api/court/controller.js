const courtModel = require('./model');
const get = require('lodash/get');
const { validationService } = require('../../helper');
async function create(req, res) {
    try {
        console.log(req.body);
        console.log("header",req.header);
        console.log('Court Deatail is', get(req, 'body', ''));
        let courtDetail = new courtModel();
        courtDetail.courtName = get(req, 'body.courtName', '');
        courtDetail.location = get(req, 'body.location', '');
        courtDetail.courtType = get(req, 'body.courtType', '');
        let validationResponse = await validationService.validation(courtDetail);
        if (validationResponse === false) {
            return Promise.reject('Validation Fail.');
        }

        let record = await courtModel.findOne({ 'courtName': get(req, 'body.courtName', '') });
        if (record) {
            return res.send('Court Name Already Present');
        }

        await courtDetail.save();
        console.log('Document Save Successfully.');
        return {
            status: 200,
            message: 'Document Save Successfully.'
        }

    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }

}

async function getRecord(req, res) {
    try {
        console.log('within get record..');
        let record = req.query.id ?
            await courtModel.findOne({ _id: req.query.id, 'flag': true }, { flag: 0 }).exec() :
            await courtModel.find({ 'flag': true }, { flag: 0 }).exec();
        if (!record) {
            return Promise.reject('Record Not Found.');
        }
        return res.status(200).json({ doc: record, message: 'Success' });

    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}
async function update(req, res) {
    try {
        console.log('Court Record TO Edit.', get(req, 'body', ''), 'Id is', get(req, 'query.id', ''));
        let validationResponse = await validationService.validation(get(req, 'body', ''));
        if (validationResponse === false) {
            return Promise.reject('Validation Fail.');
        }

        let record = await courtModel.updateOne({ '_id': req.query.id, 'flag': true },
            {
                $set: {
                    'courtName': get(req, 'body.courtName', ''),
                    'location': get(req, 'body.location', ''),
                    'courtType': get(req, 'body.courtType', '')
                }
            });
        if (!record) {
            return Promise.reject('Record Not Found.');
        }
        return {
            status: 200,
            message: 'Document Update Successful.'
        }

    } catch (error) {
        return {
            status: 500,
            message: 'Inernal Server Error.'
        }
    }
}
async function deleteRecord(req, res) {
    try {
        console.log('delete Record.', get(req, 'query.id', ''));
        await courtModel.updateOne({ '_id': req.query.id }, { $set: { 'flag': false } });
        console.log('Document Delete Successful.');
        return {
            status: 200,
            message: 'Document Delete Successful.'
        }

    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}
module.exports = {
    create,
    getRecord,
    update,
    deleteRecord
}
