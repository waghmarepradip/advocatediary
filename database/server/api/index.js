const router = require('express').Router();
const advocate = require('./advocate');
const client = require('./client');
const casemgt = require('./casemgt');
const court = require('./court');
const user = require('./user');
const permissions=require('./permissions');
const checktoken=require('../helper').checkToken;

router.use('/register', advocate.route);
router.use('/login', advocate.route);

router.use('/court', checktoken.checkToken,court.route);

router.use('/case', checktoken.checkToken,casemgt.route);

router.use('/client',checktoken.checkToken, client.route);

router.use('/user',checktoken.checkToken,user.route);

router.use('/permission',permissions.route)

module.exports = router;