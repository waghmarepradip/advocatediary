const advocateModel = require('./model');
//const caseModel = require('../casemgt/model');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const get = require('lodash/get');
const { validationService } = require('../../helper');
const { mailService } = require('../../helper');
const { emailTemplateProvider } = require('../../helper');
const { config } = require('../../core')
async function create(req, res) {
    try {

        console.log('New Registration ,User Details:', get(req, 'body', ''), "file is:", get(req, 'file', ''));
        let advocateData = new advocateModel();

        advocateData.name = get(req, 'body.name', '');
        advocateData.token = get(req, 'body.token', '')
        advocateData.email = get(req, 'body.email', '');
        advocateData.phoneNo = get(req, 'body.phoneNo', '');
        advocateData.image = get(req, 'file.filename', '');
        let hash = bcrypt.hashSync(get(req, 'body.password', ''), 10);
        advocateData.password = hash;

        let validationServiceResponse = await validationService.validation(get(req, 'body', ''));
        if (validationServiceResponse === false) {
            throw new Error('Validation Fail');
        }

        let registrationResponse = await advocateModel.findOne({ email: advocateData.email }).exec();
        if (registrationResponse) {
            return res.status(200).json({ message: 'Email Already Present.' });
        }
        let registrationSave = await advocateData.save();
        if (!registrationSave) {
            throw new Error('Data Not Save.');
        }

        let payload = { subject: registrationSave._id };
        let token = jwt.sign(payload, config.secretkey, { expiresIn: '1h' });

        const html = await emailTemplateProvider.emailTemplateProvider('userRegistration', token);

        let sendMailResponse = await mailService.sendMail('gasrv1296@gmail.com', advocateData.email,
            ' Email Verification', html);
        if (!sendMailResponse) {
            return res.status(404).json({ message: 'Email Not Found' });
        }

        return {
            status: 200,
            message: 'Registration successfully completed.'
        };

    } catch (error) {
        console.log("Error Occure in create function", error);
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    };
}
async function resendEmail(req, res) {
    try {
        console.log('Resend Mail', get(req, 'body', ''));
        let record = await advocateModel.findOne({ 'email': get(req, 'body.email', '') }).exec();
        if (!record) {
            return res.status(404).json({ message: 'Record Not Found' });
        }
        console.log('Record', record);
        let payload = { subject: record._id },
            token = jwt.sign(payload, config.secretkey);
        let html = await emailTemplateProvider.emailTemplateProvider('userRegistration', token);
        let sendMailResponse = await mailService.sendMail('gasrv1296@gmail.com', record.email,
            ' Email Verification', html);
        if (!sendMailResponse) {
            return res.status(404).json({ message: 'Email Not Found' });
        }
     
        return {
            status: 200,
            message: 'Mail Send successfully.'
        };
    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }

}

async function verifyEmail(req, res) {
    try {
        console.log('Verify id', req.query.id);
        let decoded = jwt.verify(req.query.id, config.secretkey);
        console.log("decod value ", decoded.subject);
        await advocateModel.updateOne({ '_id': decoded.subject }, { $set: { 'status': true } });

        return res.status(200).json({ message: 'Email Verify' });

    } catch (error) {
        try {
            console.log('status=>', req.query.id);
            let record = await advocateModel.updateOne({ '_id': req.query.id }, { $set: { 'status': false } });
           console.log('Record',record);
            if (!record) {
                throw new Error('Record Not Found.');
            }

            return res.status(200).json({ message: 'Inactive User' });
        } catch (error) {
            return {
                status: 500,
                message: 'Internal Server Error'
            }
        }
    }
}

async function getAllRecord(req, res) {
    try {
        console.log('Rgistration All Records', req);
        const record = req.query.id ?
            await advocateModel.findOne({ _id: req.query.id, 'flag': true }, { password: 0, flag: 0 }).exec() :
            await advocateModel.find({ flag: 'true' }, { password: 0, flag: 0 }).exec();
        if (!record) {
            return res.status(404).json({ message: 'Record Not Found.' });
        }

        return res.status(200).json({ status: 'success', message: 'success', doc: record });

    } catch (error) {
        console.log('Error occured in get all records ', error);
        return {
            status: 500,
            message: 'Internal Server Error.'
        };
    }
}

async function checkLogin(req, res) {
    try {
        console.log('Check Emai:', get(req, 'body', ''));

        let record = await advocateModel.findOne({ 'email': get(req, 'body.email', '') }).exec();
        if (!record) {
            return res.status(404).json({ message: 'Email Not present' });
        }
        let name = record.name,
            profileimg = record.image
        status = record.status

        if (record.status === false) {
            return res.status(200).json({ message: 'Verify Your Email', name: name, value: false });
        }
        console.log("record", record);

        let result = await bcrypt.compare(get(req, 'body.password', ''), record.password);
        if (result === false) {
            return res.status(404).json({ message: 'Invalid Password' });
        }
        console.log("Result:", result);

        console.log('Name:', name, 'img', profileimg);
        let payload = { subject: record.id };
        let token = jwt.sign(payload, config.secretkey);
        return res.status(200).json({ message: ' Login successfull.', doc: token, name, profileimg, status });


    } catch (error) {
        console.log('Error occured in checklogin ', error);
        return res.status(500).json({ message: 'Internal Server Error.', doc: name, status })
    }

}

async function resetPasswordLink(req, res) {
    try {
        console.log('Login Email:', get(req, 'body', ''));
        let record = await advocateModel.findOne({ email: req.body.email, 'flag': true });
        if (!record) {
            return Promise.reject('Record Not Found.');
        }
        let payload = { subject: record._id };
        let token = jwt.sign(payload, config.secretkey);
        console.log('token:', token);

        const html = await emailTemplateProvider.emailTemplateProvider('verification', token);

        let sendMailResponse = await mailService.sendMail('gasrv1296@gmail.com', record.email,
            ' Email Verification', html);
        console.log("sendMailResponse", sendMailResponse);
        return {
            status: 200,
            message: 'success.'
        }


    } catch (error) {
        console.log('Error occured in checklogin ', error);

        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }

}

async function changePassword(req, res) {
    try {
        console.log('Change Password', get(req, 'body', ''));
        let record = await advocateModel.findOne({ '_id': req.decoded.subject, 'flag': true });
        if (!record) {
            return res.status(404).message({ message: 'Record Not Found' });
        }

        let response = await bcrypt.compare(get(req, 'body.oldpass', ''), record.password);
        if (response === false) {
            return Promise.reject('Password Not Found');
        }

        let hash = bcrypt.hashSync(get(req, 'body.newpass', ''), 10)

        let updatePassResponse = await advocateModel.updateOne({ '_id': decode.subject }, { $set: { 'password': hash } });
        if (!updatePassResponse) {
            return Promise.reject('Updation Fails.');
        }
        return res.status(200).json({ message: 'Change Password Successful.' });

    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}

async function updatePassword(req, res) {
    try {
        console.log('Reset password', get(req, 'body', ''));
        let record = await advocateModel.findById({ _id: req.decoded.subject, 'flag': true });
        if (!record) {
            return res.status(404).json({ message: 'Record Not Found' });
        }
        let hash = bcrypt.hashSync(get(req, 'body.password', ''), 10);
        record.password = hash;
        await record.save();
        console.log('Password Update Successful.');

        return {
            status: 200,
            message: 'Password Update Successful.'
        }
    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }

}

async function update(req, res) {

    try {

        console.log('Update Registration ,User Details:', get(req, 'body', ''),
            'Id=>', req.params.id);
        console.log('Image File:', get(req, 'file', ''));
        if (get(req, 'file', '')) {
            console.log(get(req, 'file.filename', ''));
            let details = await advocateModel.findOne({ _id: req.params.id, 'flag': true });
            if (!details) {
                throw new Error('Record Not Found.');
            }
            details.name = details.name;
            details.email = details.email;
            details.phoneNo = details.phoneNo;
            details.image = get(req, 'file.filename', '');
            await details.save();
            return res.status(200).json({ message: 'Update Image Success.' });
        }

        let data = await advocateModel.findOne({ _id: req.params.id, 'flag': true });
        if (!data) {
            throw new Error('Record Not Found.');
        }
        let validationServiceResponse = await validationService.validation(get(req, 'body', ''));
        if (validationServiceResponse === false) {
            throw new Error('Validation Fail');
        }
        data.name = get(req, 'body.name', '');
        data.email = get(req, 'body.email', '');
        data.phoneNo = get(req, 'body.phoneNo', '');

        await data.save();

        return {
            status: 200,
            message: 'success.'
        };
    } catch (error) {
        console.log('Error occured in get all records ', error);
        return {
            status: 500,
            message: 'Internal Server Error.'
        };
    }
}

module.exports = {
    create,
    getAllRecord,
    checkLogin,
    resetPasswordLink,
    updatePassword,
    update,
    verifyEmail,
    changePassword,
    resendEmail

}