const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var advocateSchema = new Schema({
    name: String,
    token: String,
    phoneNo: Number,
    email: {
        type: String,
        required: true
    },
    address: String,
    advocateskills: Array,
    password: {
        type: String,
        required: true
    },
    image: String,
    flag: {
        type: Boolean,
        default: true
    },
    status: {
        type: Boolean,
        default: false
    },
    permssion: {type: Number, required: true, default: 1}

});

module.exports = mongoose.model('advocateSchema', advocateSchema);