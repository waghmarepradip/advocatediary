const casemodel = require('./model');
const get = require('lodash/get');
const { validationService } = require('../../helper');
const mongoose = require('mongoose');
const { mailService }=require('../../helper');
const { emailTemplateProvider } = require('../../helper');

let today = new Date(),
    tomorrow = new Date(today)
tomorrow.setDate(today.getDate() + 1),
    dayaftertomorrow = new Date(tomorrow)
dayaftertomorrow.setDate(tomorrow.getDate() + 1),
    lastDay = new Date(today.getFullYear(), today.getMonth() + 1 , 0);
async function create(req, res) {
    try {
        console.log('Case Deatil.', get(req, 'body', ''));

        let caseData = new casemodel();
        caseData.caseId = get(req, 'body.caseId', '');
        caseData.caseName = get(req, 'body.caseName', '');
        caseData.advocateId = req.decoded.subject;
        caseData.courtId = get(req, 'body.courtId', '');
        caseData.caseType = get(req, 'body.caseType', '');
        caseData.nextDate = get(req, 'body.nextDate', '');
        caseData.clientId = get(req, 'body.clientId', '');
        caseData.oppositeParty = get(req, 'body.oppositeParty', '');

        let validationResponse = await validationService.validation(caseData);
        if (validationResponse === false) {
            return res.send('Validation Fails.');
        }

        let response = await casemodel.findOne({ 'caseId': get(req, 'body.caseId', '') });
        if (response) {
            return res.status(200).json({ message: 'Case Id Already Present.' });
        }
        await caseData.save();

        return res.status(200).json({ message: 'Record Save Successfully.' });

    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}

async function getRecord(req, res) {
    try {
        console.log('Case Records.', req.query.id);
        let oneCaseRecord = await casemodel.findOne({ _id: req.query.id, 'flag': true });
        return res.status(200).json({ message: 'success', doc: oneCaseRecord });

    } catch (error) {
        try {
            console.log('AdvocateId =>',req.decoded.subject);
            let response = await casemodel.find({'advocateId': req.decoded.subject ,'flag': true})
            return res.status(200).json({ message: 'success', doc: response });
        } catch (error) {
            return {
                status: 500,
                message: 'Internal Server Error.'
            }

        }

    }
}

async function update(req, res) {
    try {
        console.log('Case Data Updates', get(req, 'body', ''), get(req, 'params.id', ''));
        let record = await casemodel.findById({ '_id': req.params.id, 'flag': true });
        if (!record) {
            return res.status(404).json({ message: 'Record Not Found.' });
        }
        
        let validationResponse = await validationService.validation(get(req, 'body', ''));
        if (validationResponse === false) {
            return res.send('Validation Fails.');
        }

        record.caseId = get(req, 'body.caseId', '');
        record.caseName = get(req, 'body.caseName', '');
        record.userId = get(req, 'body.userId', '');
        record.courtId = get(req, 'body.courtId', '');
        record.caseType = get(req, 'body.caseType', '');
        record.oppositeParty = get(req, 'body.oppositeParty', '');

        let response = await casemodel.updateOne({ '_id': req.params.id }, { $set: record });
        if (!response) {
            return res.status(404).json({ message: 'Record Not Found.' });
        }

       return res.status(200).json({ message: 'Data Update Successful.' });

    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}

async function deleteRecord(req, res) {
    try {
        console.log('Delete Id', req.params.id);
       let response= await casemodel.findOne({ '_id': req.params.id , 'flag':true});
     
       response.flag=false;
       await response.save();
        return {
            status: 200,
            message: ' Document Deleted Successfully.',
            doc:response
        }
    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}


async function addReview(req, res) {
    try {
        console.log('Add Review:', get(req, 'body', ''), get(req, 'params.id', ''));
       let response= await casemodel.updateOne({ _id: get(req, 'params.id', '') },
            {
                $push: { review: get(req, 'body.review', '') },
                $set: { nextDate: get(req, 'body.nextDate', '') }
            });
            console.log(req.body.advocateEmailId,req.body.clientEmailId);
            const html = await emailTemplateProvider.emailTemplateProvider('sendNextDate',req.body.nextdate);
  
            let sendmailResponse=await mailService.sendMail(req.body.advocateEmailId,req.body.clientEmailId,'Case Next Date',html);
            if(!sendmailResponse){
                return res.status(200).json({message:'Mail Not Send.'});
            }
            console.log('sendmailResponse',sendmailResponse);
            return res.status(200).json({message:'Mail Send Successfully.'})

    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}
async function deleteReview(req, res) {
    try {
        console.log('Delete Review Record:', get(req, 'params.id', ''));
        let response = await casemodel.updateOne({ "review": { $elemMatch: { '_id': get(req, 'params.id', '') } } },
            {
                $set: {
                    'review.$.flagdelete': false,
                }
            });
        console.log('Response:', response);
        return {
            status: 200,
            message: 'Review Delete Successful.'
        }

    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}

async function getTotalCaseCount(req, res) {
    try {
        console.log('getTotalCount', get(req, 'params.id', ''));
        let totalCaseCount = await casemodel.find({ 'advocateId': req.decoded.subject ,flag:true}).count();
        if (!totalCaseCount) {
            return res.status(404).json({ message: 'Record Not Found' });
        }
      
        let todayCaseCountRes = await casemodel.aggregate([{
            $match: {
                $and: [{ advocateId: mongoose.Types.ObjectId(req.decoded.subject) ,flag:true},
                {
                    nextDate: {
                        $lt: tomorrow,
                        $gte: today
                    }
                }]
             }
        },
        {
            $group: {
                     _id: { year: { $year: "$nextDate" }, month: { $month: "$nextDate" } },
                      count: { $sum: 1}
                    } 
        }
        ])
        // if(todayCaseCountRes<1){
        //     todayCaseCountRes=0;
        // }
   
        let tomorrowCaseCountRes = await casemodel.aggregate([{
            $match: {
                     $and: [{ advocateId: mongoose.Types.ObjectId(req.decoded.subject) , flag:true},
                            {
                              nextDate: {
                                         $lt: dayaftertomorrow,
                                         $gte: tomorrow
                                        }
                            }]
                    }   
        },
        {
            $group: {
                _id: { year: { $year: "$nextDate" }, month: { $month: "$nextDate" } },
                count: {
                    $sum: 1
                }
            }
        }

        ])
        // if(tomorrowCaseCountRes<1){
        //     tomorrowCaseCountRes=0;
        // }
        let monthCaseCountRes = await casemodel.aggregate(
            [{
                $match: {
                    $and: [{ advocateId: mongoose.Types.ObjectId(req.decoded.subject) , flag:true},
                    {
                        nextDate: {
                            $lte: lastDay,
                            $gt: today

                        }
                    },
                    ]

                }
            },
            {
                $group: {
                    _id: { year: { $year: "$nextDate" }, month: { $month: "$nextDate" } },
                    count: { $sum: 1 }
                }
            },

            ]);
       
            if(monthCaseCountRes<1){
                monthCaseCountRes=0;
            }
        return res.status(200).json({ doc: { 'totalCaseCount': totalCaseCount, 'todayCaseCount': todayCaseCountRes, 'tommorowCaseCount': tomorrowCaseCountRes, 'monthlyCaseCount': monthCaseCountRes } });

    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}

async function getTodayCase(req, res) {
    try {
        console.log('Today Case Detail:', get(req, 'params.id', ''));
    
        let record = await casemodel.find({
            $and: [
                { 'advocateId': req.decoded.subject, 'flag':true },
                {
                    'nextDate': {
                        $lte: tomorrow,
                        $gte: today
                    }
                }
            ]

        }, { flag: 0 });
        if (!record) {
            throw new Error('Record Not Found.');
        }
        return res.status(200).json({ doc: record });

    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'

        }
    }
}
async function getTomorrowCase(req, res) {
    try {
        console.log('Tomorrow Case Detail:', get(req, 'params.id', ''));
        let record = await casemodel.find({
            $and: [
                { 'advocateId': req.decoded.subject, 'flag':true },
                {
                    'nextDate': {
                        $lt: dayaftertomorrow,
                        $gte: tomorrow
                    }
                }
            ]

        }, { flag: 0 });
        if (!record) {
            throw new Error('Record Not Found.');
        }
        return res.status(200).json({ doc: record });

    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'

        }
    }
}

async function getMonthlyCase(req, res) {
    try {
        console.log('Monthly Record',req.decoded.subject);

        let record = await casemodel.find({
            $and: [
                { 'advocateId': req.decoded.subject , 'flag':true},
                {
                    'nextDate': {
                        $lte: lastDay,
                        $gt: today
                    }
                }
            ]
        }, { flag: 0 });
       
        return res.status(200).json({ doc: record });
        
    } catch (error) {
        return {
            status: 500,
            message: 'Internal Server Error.'
        }
    }
}

module.exports = {
    create,
    getRecord,
    update,
    deleteRecord,
    addReview,
    deleteReview,
    getTotalCaseCount,
    getTodayCase,
    getTomorrowCase,
    getMonthlyCase,
    

}