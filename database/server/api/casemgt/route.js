const router = require('express').Router();
const controller = require('./controller');


async function post(req, res) {
    console.log('Post');
    let response = await controller.create(req, res);
    res.status(response.status).json({ message: response.message });
}

async function get(req, res) {
    console.log('Get');
    let response = await controller.getRecord(req, res);
    res.status(response.status).json({ message: response.message });
}

async function put(req, res) {
    console.log('Put');
    let response = await controller.update(req, res);
    res.status(response.status).json({ message: response.message });
}
async function deleteRecord(req, res) {
    console.log('delete');
    let response = await controller.deleteRecord(req, res);
    res.status(response.status).json({ message: response.message });
}

async function putreview(req, res) {
    console.log('PostReview:');
    let response = await controller.addReview(req, res);
    res.status(response.status).json({ message: response.message });
}
async function deletereview(req, res) {
    console.log('Delete Review.');
    let response = await controller.deleteReview(req, res);
    res.status(response.status).json({ message: response.message });
}
async function gettotalcase(req, res) {
    console.log('total case count');
    let response = await controller.getTotalCaseCount(req, res);
    res.status(response.status).json({ message: response.message });
}
async function getTodayCase(req, res) {
    console.log('Get Today Case');
    let response = await controller.getTodayCase(req, res);
    res.status(response.status).json({ message: response.message });
}
async function getTomorrowCase(req, res) {
    console.log('Get Tomorrow Case:');
    let response = await controller.getTomorrowCase(req, res);
    res.status(response.status).json({ message: response.message });
}
async function getMonthlyCase(req, res) {
    console.log('Get Monthly Case Record');
    let response = await controller.getMonthlyCase(req, res);
    res.status(response.status).json({ message: response.message });
}



router.get('/?',get);
router.post('/',post);
router.delete('/:id', deleteRecord);
router.put('/:id', put);

router.get('/totalcase/:id', gettotalcase);
router.get('/todaycase/:id', getTodayCase);
router.get('/tomorrowcase/:id', getTomorrowCase);
router.get('/monthlycase/:id', getMonthlyCase);

router.put('/review/:id', putreview);
router.delete('/review/:id', deletereview);



module.exports = router;
