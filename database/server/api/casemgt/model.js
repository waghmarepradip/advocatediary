const mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
const autopopulateVar=require('mongoose-autopopulate');
const Schema = mongoose.Schema;

const caseSchema = new Schema({
    caseId: {
        type: String,
        required: true
    },
    caseName: {
        type: String,
        required: true
    },
    oppositeParty: Array,
    clientId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "clientSchema",
        autopopulate:{
          select:'name email -_id'
        }
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "userSchema",
        autopopulate:{
            select:'name -_id'
        }
    },
    advocateId: {
        type: Schema.Types.ObjectId,
        ref: "advocateSchema",
        autopopulate:{
            select:'name email -_id'
        }
    },
    courtId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "courtSchema",
        autopopulate:{
            select:'courtName -_id'
        }
    },

    caseType: String,
    caseStatus: {
        type: String,
        default: "Processing"
    },
    flag: {
        type: Boolean,
        default: true
    },
    nextDate: Date,
    review: [{
        date: Date,
        remark: String,
        flagdelete: {
            type: Boolean,
            default: true
        }
    }]
});

caseSchema.plugin(timestamps);
caseSchema.plugin(autopopulateVar);
module.exports = mongoose.model('caseSchema', caseSchema);