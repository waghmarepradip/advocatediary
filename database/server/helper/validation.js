const validator = require('validator');

async function validation(data) {
    console.log('Validation Function', data);

    if (data.caseId) {
        console.log('Validation Function', data.caseId, data.caseName);
        let caseDatavalidate = {
            caseId: validator.isAlphanumeric(data.caseId),
            caseName: validator.matches(data.caseName, '^[a-zA-Z][a-zA-Z\\s]+$')
        }
        console.log('case Data', caseDatavalidate);
        if (caseDatavalidate.caseId || caseDatavalidate.caseName) {
            return caseDatavalidate;
        } else
            return false;

    } else if (data.courtName) {
        let courtData = {
            courtName: validator.matches(data.courtName, '^[a-zA-Z][a-zA-Z\\s]+$'),
            location: validator.matches(data.courtName, '^[a-zA-Z][a-zA-Z\\s]+$')
        }
        console.log('court valid data', courtData);
        if (courtData.location && courtData.courtName) {
            return courtData;
        } else
            return false;

    } else {
        let Data = {
            name: validator.matches(data.name, '^[a-zA-Z][a-zA-Z\\s]+$'),
            email: validator.isEmail(data.email),
            phoneNo: validator.isMobilePhone(data.phoneNo),
        }
        console.log(' data', Data);
        console.log("Email=>", Data.email, "name=>", Data.name, "phoneNo", Data.phoneNo);

        if (Data.name && Data.email) {
            return Data;
        } else
            return false;
    }
}
module.exports = {
    validation
}